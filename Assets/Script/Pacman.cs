using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Pacman : MonoBehaviour
{
    private Rigidbody rb;
    private float x;
    public GameObject myPrefab;
    private GameObject G1;
    private GameObject G2;
    private GameObject G3;
    public GameObject GO;

    // Start is called before the first frame update
    private void Awake()
    {
        GO.SetActive(false);

    }
    void Start()
    {
        
        G1 = Instantiate(myPrefab, new Vector3(3, 0, 7), Quaternion.identity);
        G1.gameObject.name = "food";
        G2 = Instantiate(myPrefab, new Vector3(3, 0, 5), Quaternion.identity);
        G2.gameObject.name = "food";
        G3 = Instantiate(myPrefab, new Vector3(5, 0, 4), Quaternion.identity);
        G3.gameObject.name = "food";
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        Screen.fullScreen = true;
        rb.velocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        rb.velocity *= 2.1f;
    }
    private void OnCollisionEnter(Collision collision)
    {  if(collision.gameObject.name.Equals("Ghost"))
        {
            Destroy(gameObject);
            GO.SetActive(true);
            
        }

        if (collision.gameObject.name.Equals("food"))
        { 
            Destroy(collision.gameObject);
            G1 = Instantiate(myPrefab, new Vector3(Random.Range(-8, 6), 0, Random.Range(-7, 9)), Quaternion.identity);
            G1.gameObject.name = "food";
            G3 = Instantiate(myPrefab, new Vector3(Random.Range(-8, 6), 0, Random.Range(-7, 9)), Quaternion.identity); ;
            G3.gameObject.name = "food";
            ScoreManager.instance.addscore();



        }

    }
}
