using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour

{
    [SerializeField] private float selfrotate = 1f;
    private float selfangle = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, selfangle * Time.deltaTime, 0f);
        selfangle += selfrotate * Time.deltaTime;
    }
}
