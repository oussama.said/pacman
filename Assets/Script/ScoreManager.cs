using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text score;
    public Text hightscore;
    public static ScoreManager instance;
    int points = 0;
    int hscore = 0;
    // Start is called before the first frame update
    void Start()
    {
        score.text = " Score : "+ points.ToString() + " Pts";
        hscore = PlayerPrefs.GetInt("hightscore", 0);
        hightscore.text = "HighScore : " + hscore.ToString();

    }
    private void Awake()
    {
        instance = this;    
    }
    // Update is called once per frame
    public void addscore()
    {
        points += 1;
        score.text = " Score : " + points.ToString() + " Pts";
        if (hscore < points)
         PlayerPrefs.SetInt("hightscore", points);
        PlayerPrefs.Save();


    }
}
